import { Component, OnInit } from '@angular/core';
import {Route, Router} from "@angular/router";
import {LoginService} from "../shared/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  constructor(private router: Router,
              private loginService: LoginService) { }

  ngOnInit() {
  }

  goToSearch(): void {
    this.loginService.setLoginIn(true);
    this.router.navigate(['/search']);
  }

}
