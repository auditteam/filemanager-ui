import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  autoSearchForm: FormGroup;

  employeeData = [
    'One',
    'Two',
    'Three'
  ];
  constructor(private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.autoSearchForm = this.formBuilder.group( {
        employeeKeyword:['', Validators.required]
    });
  }

  goToEmployeeDetails(): void {
    this.router.navigate(['/employee-details']);
  }

}
