import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
  loginIn: boolean;
  constructor() { }

  getLoginIn(): boolean {
    return this.loginIn;
  }

  setLoginIn(loginIn: boolean): void {
    this.loginIn = loginIn;
  }
}
